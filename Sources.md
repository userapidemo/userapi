### Mockery: Some important commands
    - mockery --name=[Individual interface name]
        create mock file for that particular interface
    - mockery --all
        create mock files of all the interfaces for the same package but not sub-package
    - mockery --all --recursive==true
        create mock files of all the interfaces for the same and sub-packages in a folder called mock
    - mockery --all --inpackage
        create mock files of all the interfaces for the same and sub-packages in a location where interface located

#### Integration tastes using stubs instead of mock
    https://articles.microservices.com/practical-microservices-integration-tests-and-stub-services-80749ce01050
    https://martinfowler.com/articles/mocksArentStubs.html
#### Integrations tastes part 1 & 2
    https://www.ardanlabs.com/blog/2019/03/integration-testing-in-go-executing-tests-with-docker.html
    https://www.ardanlabs.com/blog/2019/10/integration-testing-in-go-set-up-and-writing-tests.html
#### BDD and Integration Tests(BOSS)
    https://medium.com/m/global-identity?redirectUrl=https%3A%2F%2Flevelup.gitconnected.com%2Fimplementation-of-behavioral-driven-development-bdd-using-golang-d33073243722
    https://www.cloudbees.com/blog/implementing-a-bdd-workflow-in-go
    http://goconvey.co/
    https://www.cloudbees.com/blog/implementing-a-bdd-workflow-in-go
    https://medium.com/m/global-identity?redirectUrl=https%3A%2F%2Farticles.microservices.com%2Fpractical-microservices-integration-tests-and-stub-services-80749ce01050
#### BDD vs Integration(Me)
    https://medium.datadriveninvestor.com/introduction-to-bdd-and-integration-test-b536625d17dd
    

#### Redis CLI
    docker exec -it redis-cache redis-cli
    keys *