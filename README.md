#DemoApp
##About Project
- --
- This project all about user information
- We have total 4 services
  - **address service:** stores and retrieves user address info
  - **phone service:** stores and retrieves user contact info
  - **user service:** stores and retrieves user identity info
  - **userapi:** this is REST service which interacts with rpc endpoints
##Used Tools
  - --
  - Postgres
  - Redis
  - grpc
  - rest
  - protobuf
  - go-chi/chi etc
##How to run?
  - --
> >Locally ```go run cmd/main.go```
> 
> >In docker ```docker compose up```  
> 
> Above commands will do everything for you
> 
>**Note:** We set up resources by using docker so at first you need to create those resources. So for the first time
>just run ```docker compose up --build```. It will create services images also.
> 
>**Note:** Since we don't use any ORM tool so please run all the sql scripts through dbeaver or something like this. To connect
>with db see configuration in **docker-compose.yml** file.
##Setup goconvey
  - --
  - Install goconvey 
    ~~~
    go get github.com/smartystreets/goconvey or 
    go install github.com/smartystreets/goconvey
    ~~~
  - Run ```goconvey``` where you have your tests, in our case see userapi/internal/handler
##Setup plantuml
  - --
  - Install goplantuml
    ~~~
      go get github.com/jfeliu007/goplantuml/parser
      go get github.com/jfeliu007/goplantuml/cmd/goplantuml
    ~~~
  - Generate puml file, suppose below is your project directory and your terminal path ../user
> - user
>   - diagrams
>   - internal 
>    - data
>    - handler
  - Generate puml for data layer: ```goplantuml ./internal/data ./diagrams > diagrams/diagram.puml``` It will generate diagram.puml file in diagrams folder
  - Generate puml for multiple layer: ```goplantuml ./internal/data ./internal/handler ./diagrams > diagrams/diagram.puml```
  - And to see as image copy code from diagram.puml and paste it [here](http://www.plantuml.com/plantuml/uml/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000)