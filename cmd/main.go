package main

import (
	"bitbucket.org/userapidemo/userapi/internal/handler"
	rtr "bitbucket.org/userapidemo/userapi/internal/router"
	"fmt"
	"github.com/asim/go-micro/v3"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"net/http"
)

func main() {
	router := initRouter()
	service := micro.NewService()
	h := handler.NewUserHandler(service.Client())
	rtr.Routers(h, router)
	fmt.Println("USERAPI is now listening to : http://localhost:8090")
	err := http.ListenAndServe(":8090", Logger(router))
	if err != nil {
		fmt.Println("failed to start server with error: ", err)
	}
}

func Logger(router *chi.Mux) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//fmt.Println(time.Now(), r.Method, r.URL)
		router.ServeHTTP(w, r) // dispatch the request
	})
}

func initRouter() *chi.Mux {
	Router := chi.NewRouter()
	Router.Use(middleware.Recoverer)
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	Router.Use(cors.Handler)
	return Router
}
