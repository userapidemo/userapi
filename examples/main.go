package main

import (
	proto "bitbucket.org/userapidemo/user/pkg/generated/api"
	"context"
	"fmt"
	"github.com/asim/go-micro/v3"
	"github.com/asim/go-micro/v3/util/log"
)

func main() {
	client := micro.NewService()
	service := proto.NewUserService("user-service", client.Client())
	res, err := service.CreateUser(context.Background(), &proto.UserRequest{
		Name: "User-Api-Demo",
	})
	if err != nil {
		log.Fatal("Got error ==> CreateUser: ", err)
	}
	fmt.Println(res.Status)
}
