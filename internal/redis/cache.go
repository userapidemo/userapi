package redis

import (
	"bitbucket.org/userapidemo/userapi/internal/redis/connection"
	"bitbucket.org/userapidemo/userapi/internal/redis/model"
	"encoding/json"
	"fmt"
	"github.com/asim/go-micro/v3/util/log"
	"github.com/go-redis/redis"
)

const (
	key = "user"
)

type Redis interface {
	Store(user *model.User) error
	Get(userId int32) (*model.User, error)
	Delete(userId int32) (int64, error)
}

func GetNewRedisCache() Redis {
	return &cache{
		RClient: connection.CacheClient(),
	}
}

type cache struct {
	RClient *redis.Client
}

func (c *cache) Store(user *model.User) error {
	userkey := fmt.Sprintf(key+"-%d", user.UserId)
	bytes, err := json.Marshal(user)
	if err != nil {
		log.Log("Failed to unmarshal")
		return err
	}
	err = c.RClient.Set(userkey, bytes, 0).Err()
	if err != nil {
		return err
	}
	return nil
}
func (c *cache) Get(userId int32) (*model.User, error) {
	var user model.User
	userkey := fmt.Sprintf(key+"-%d", userId)
	val, err := c.RClient.Get(userkey).Bytes()
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(val, &user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func (c *cache) Delete(userId int32) (int64, error) {
	userKey := fmt.Sprintf(key+"-%d", userId)
	val, err := c.RClient.Del(userKey).Result()
	if err != nil {
		return val, err
	}
	return val, nil
}
