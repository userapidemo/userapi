package connection

import (
	"github.com/go-redis/redis"
)

func CacheClient() *redis.Client {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})
	return rdb
}
