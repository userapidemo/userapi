package converter

import (
	addressApi "bitbucket.org/userapidemo/address/pkg/generated/api"
	phoneApi "bitbucket.org/userapidemo/phone/pkg/generated/api"
	userApi "bitbucket.org/userapidemo/user/pkg/generated/api"
	"bitbucket.org/userapidemo/userapi/pkg/generated/api"
)

type MapperService interface {
	ApiToDomains(request *api.UserCreateRequest) (*userApi.UserRequest, *addressApi.AddressRequest, *phoneApi.PhoneRequest)
}

type converter struct{}

func NewConverterService() MapperService {
	return &converter{}
}

func (c *converter) ApiToDomains(request *api.UserCreateRequest) (*userApi.UserRequest, *addressApi.AddressRequest, *phoneApi.PhoneRequest) {
	if request == nil {
		return nil, nil, nil
	}
	user := &userApi.UserRequest{Name: request.User.Name}
	address := &addressApi.AddressRequest{City: request.Address.City, Province: request.Address.Province,
		Country: request.Address.Country}
	phone := &phoneApi.PhoneRequest{Phone: request.Phone.Phone, Type: request.Phone.Type}
	return user, address, phone
}
