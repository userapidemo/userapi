package handler

import (
	"bitbucket.org/userapidemo/userapi/internal/converter"
	"bitbucket.org/userapidemo/userapi/internal/redis"
	"bitbucket.org/userapidemo/userapi/internal/redis/model"
	"bitbucket.org/userapidemo/userapi/internal/svc"
	"bitbucket.org/userapidemo/userapi/pkg/generated/api"
	"encoding/json"
	"fmt"
	"github.com/asim/go-micro/v3/client"
	"github.com/asim/go-micro/v3/util/log"
	"github.com/go-chi/chi"
	"github.com/gogo/protobuf/jsonpb"
	"net/http"
	"strconv"
)

type UserHttpHandler interface {
	CreateUser(w http.ResponseWriter, r *http.Request)
	UpdateUser(w http.ResponseWriter, r *http.Request)
	GetUser(w http.ResponseWriter, r *http.Request)
	DeleteUser(w http.ResponseWriter, r *http.Request)
}

type handler struct {
	client svc.Clients
	mapper converter.MapperService
	redis  redis.Redis
}

func NewUserHandler(client client.Client) UserHttpHandler {
	return &handler{
		client: svc.NewServiceClient(client),
		mapper: converter.NewConverterService(),
		redis:  redis.GetNewRedisCache(),
	}
}

func (h *handler) CreateUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var in api.UserCreateRequest
	if err := jsonpb.Unmarshal(r.Body, &in); err != nil {
		fmt.Println(err)
		h.respondwithJSON(w, http.StatusBadRequest, "{\"response\":\"CreateUser===> bad request\"}")
		return
	}

	user, address, phone := h.mapper.ApiToDomains(&in)
	userResp, err := h.client.User().CreateUser(ctx, user)
	if err != nil {
		h.respondwithJSON(w, http.StatusBadRequest, "{\"response\":\"CreateUser===> failed to create user\"}")
		log.Log(err)
	}
	address.UserId = userResp.GetUid()
	phone.UserId = userResp.GetUid()
	addressResp, err := h.client.Address().CreateAddress(ctx, address)
	if err != nil {
		h.respondwithJSON(w, http.StatusBadRequest, "{\"response\":\"CreateUser===> failed to create address\"}")
		log.Log(err)
	}
	phoneResp, err := h.client.Phone().CreatePhone(ctx, phone)
	out := api.UserCreateResponse{
		UserId:    userResp.Uid,
		AddressId: addressResp.AddressId,
		PhoneId:   phoneResp.PhoneId,
	}
	if err != nil {
		h.respondwithJSON(w, http.StatusBadRequest, "{\"response\":\"CreateUser===> failed to create phone\"}")
		log.Log(err)
	}

	err = h.redis.Store(&model.User{out.UserId, out.AddressId, out.PhoneId})
	if err != nil {
		panic(err)
	}
	h.respondwithJSON(w, http.StatusCreated, out)
}

func (h *handler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	//TODO implement me
	panic("implement me")
}

func (h *handler) GetUser(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "userId")
	userId, err := strconv.Atoi(id)
	if err != nil {
		h.respondwithJSON(w, http.StatusBadRequest, "{\"response\":\"User===> Invalid url param\"}")
	}
	user, err := h.redis.Get(int32(userId))
	if err != nil {
		h.respondwithJSON(w, http.StatusNotFound, "{\"response\":\"User===> Not found\"}")
		log.Log(err)
	}
	h.respondwithJSON(w, http.StatusOK, user)
}

func (h *handler) DeleteUser(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "userId")
	userId, err := strconv.Atoi(id)
	if err != nil {
		h.respondwithJSON(w, http.StatusBadRequest, "{\"response\":\"User===> Invalid url param\"}")
	}
	result, err := h.redis.Delete(int32(userId))
	if err != nil {
		h.respondwithJSON(w, http.StatusBadRequest, "{\"response\":\"Cannot delete\"}")
		log.Log(err)
	}
	if result == 1 {
		h.respondwithJSON(w, http.StatusOK, "success")
	} else { //if result == 0
		h.respondwithJSON(w, http.StatusNotFound, "not found")
	}
}

func (h *handler) respondwithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
