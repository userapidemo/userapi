package handler_test

import (
	model2 "bitbucket.org/userapidemo/userapi/internal/redis/model"
	"bitbucket.org/userapidemo/userapi/pkg/generated/api"
	"encoding/json"
	"fmt"
	"github.com/gogo/protobuf/jsonpb"
	. "github.com/smartystreets/goconvey/convey"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"testing"
)

//Keep in mind that everything should be up and running
func TestDocumentation(t *testing.T) {
	path := "http://localhost:8090/user"
	requestPath := fmt.Sprintf(path + "/create-user")
	createdUser := int32(0)
	user := `{
		"user":{
			"name":"Manik"
		},
		"address":{
			"city":"Khilkhet",
			"province":"Dhaka",
			"country":"Bangladesh"
		},
		"phone":{
			"phone":"2365987",
			"type":"Land"
		}
	}`
	Convey("CreateUser", t, func() {
		inBody := strings.NewReader(user)
		Convey("When trying to generate new request", func() {
			request, err := http.NewRequest("POST", requestPath, inBody)
			Convey("Then request should be created without panic", func() {
				So(err, ShouldBeNil)
			})
			Convey("When trying to send the request", func() {
				rsp, err := http.DefaultClient.Do(request)
				Convey("The request should be successful", func() {
					So(err, ShouldBeNil)
				})
				rspBody, err := ioutil.ReadAll(rsp.Body)
				Convey("Response body should be converted into bytes", func() {
					So(err, ShouldBeNil)
				})
				user := api.UserCreateResponse{}
				err = jsonpb.Unmarshal(strings.NewReader(string(rspBody)), &user)
				Convey("Bytes should be converted into target model", func() {
					So(err, ShouldBeNil)
				})
				Convey("UserId should not be zero", func() {
					So(user.UserId, ShouldNotBeZeroValue)
				})
				createdUser = user.UserId
			})
		})
	})
	Convey("GetUser", t, func() {
		requestPath = fmt.Sprintf(path+"/get-user/%d", createdUser)
		Convey("When trying to generate new request", func() {
			request, err := http.NewRequest("GET", requestPath, nil)
			Convey("Then request should be created without panic", func() {
				So(err, ShouldBeNil)
			})
			Convey("When trying to send the request", func() {
				rsp, err := http.DefaultClient.Do(request)
				Convey("The request should be successful", func() {
					So(err, ShouldBeNil)
				})
				rspBody, err := ioutil.ReadAll(rsp.Body)
				Convey("Response body should be converted into bytes", func() {
					So(err, ShouldBeNil)
				})
				user := model2.User{}
				err = json.Unmarshal(rspBody, &user)
				Convey("Bytes should be converted into target model", func() {
					So(err, ShouldBeNil)
				})
				Convey("Model should not be nil and userId should be: "+strconv.FormatInt(int64(createdUser), 10), func() {
					So(user, ShouldNotBeNil)
					So(user.UserId, ShouldEqual, createdUser)
				})
			})
		})
	})
	Convey("Delete User", t, func() {
		requestPath = fmt.Sprintf(path+"/delete-user/%d", createdUser)
		Convey("Generating new request to delete user: "+strconv.FormatInt(int64(createdUser), 10), func() {
			request, err := http.NewRequest("DELETE", requestPath, nil)
			Convey("Then request should be created without panic", func() {
				So(err, ShouldBeNil)
			})
			Convey("When trying to send the request", func() {
				_, err := http.DefaultClient.Do(request)
				Convey("Then the request should be successful", func() {
					So(err, ShouldBeNil)
				})
				//rspBody, err := ioutil.ReadAll(rsp.Body)
				//Convey("Response body should be converted into bytes", func() {
				//	So(err, ShouldBeNil)
				//})
				//msg := ""
				//err = json.Unmarshal(rspBody, &msg)
				//Convey("Response message should be: 'success', and error should be nil", func() {
				//	So(err, ShouldBeNil)
				//	So(msg, ShouldEqual, "success")
				//})
			})
		})
	})
}
