package router

import (
	"bitbucket.org/userapidemo/userapi/internal/handler"
	"github.com/go-chi/chi"
)

func Routers(handler handler.UserHttpHandler, router *chi.Mux) *chi.Mux {
	router.Group(func(router chi.Router) {
		router.Route("/user", func(router chi.Router) {
			router.Post("/create-user", handler.CreateUser)
			router.Put("/update-user/{userId}", handler.UpdateUser)
			router.Get("/get-user/{userId}", handler.GetUser)
			router.Delete("/delete-user/{userId}", handler.DeleteUser)
		})
	})
	return router
}
