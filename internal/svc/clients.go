package svc

import (
	addressSvc "bitbucket.org/userapidemo/address/pkg/generated/api"
	phoneSvc "bitbucket.org/userapidemo/phone/pkg/generated/api"
	userSvc "bitbucket.org/userapidemo/user/pkg/generated/api"
	"github.com/asim/go-micro/v3/client"
)

const (
	UserServiceName    = "sf.user.service"
	AddressServiceName = "sf.address.service"
	PhoneServiceName   = "sf.phone.service"
)

type Clients interface {
	User() userSvc.UserService
	Address() addressSvc.AddressService
	Phone() phoneSvc.PhoneService
}
type serviceClient struct {
	user    userSvc.UserService
	address addressSvc.AddressService
	phone   phoneSvc.PhoneService
}

func NewServiceClient(client client.Client) Clients {
	svcClient := &serviceClient{}
	svcClient.user = userSvc.NewUserService(UserServiceName, client)
	svcClient.address = addressSvc.NewAddressService(AddressServiceName, client)
	svcClient.phone = phoneSvc.NewPhoneService(PhoneServiceName, client)
	return svcClient
}

func (c *serviceClient) User() userSvc.UserService {
	return c.user
}

func (c *serviceClient) Address() addressSvc.AddressService {
	return c.address
}

func (c *serviceClient) Phone() phoneSvc.PhoneService {
	return c.phone
}
