package user_api

type Stringer interface {
	String(string) string
}

type handler struct{}

func (h *handler) String(name string) string {
	return "Hello " + name + "!!"
}
